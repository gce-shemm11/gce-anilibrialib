// document.addEventListener('DOMContentLoaded', ()=>{
    "use strict";
    // получение данных со страницы
    const url = window.location.href
    const body = document.querySelector('body')
    const head = document.querySelector('head')
    let title
    if(document.querySelector('.release-title')){
        title = document.querySelector('.release-title').textContent
    }
   
    document.body.style.backgroundColor = 'orange';



    //  Создание кнопки "добавить"
    const addToLibreryBtn = document.createElement('button');
    addToLibreryBtn.textContent = 'Добавить';
    addToLibreryBtn.classList.add('addToLibrery');
    if(document.querySelector('.tab-content')){
        document.querySelector('.tab-content').append(addToLibreryBtn);
    }
    
    // Работа с кнопкой "добавить"

    addToLibreryBtn.addEventListener('click', (e) => {
        e.preventDefault()
        addAnimeToLibrery();
    });

    // Добавление аниме в библеотку LocalStorage
    function addAnimeToLibrery() {
        let titleSave = `-anit${title}`
        titleSave = titleSave.replaceAll('\n', '')
        let anime = {
            "name": titleSave,
            "src": url
        };
        let name = anime.name.replace('-anit', '')
        showAnimeInPopup(name, anime.src)
        deleteAnimeFromLibrery()
        anime = JSON.stringify(anime);
        localStorage.setItem(titleSave, anime);
    }

    // Обновление модального окна после добавления аниме в local storage

    // Получение данных с LocalStorage
    function getAnimeFromLibrery() {
        for (let i = 0; i < localStorage.length; i++) {
            let key = localStorage.key(i);
            try {
                let lsItem = localStorage.getItem(key)
                let anime = JSON.parse(lsItem);
                if (anime && anime.name){
                    if (anime.name.includes('-anit')){
                        let name = anime.name.replace('-anit', '')
                        showAnimeInPopup(name, anime.src);
                    }

                }
            } catch (error) {
            }
        }
    }
    // Показать список аниме в popup
    function showAnimeInPopup(name, src) {
        let place = document.querySelector('.modal__content');
        place.innerHTML += `
            <div class="placeAnime">
                <a href="${src}" class="animeSlot" style="padding: 0 0 0 15px;">${name}</a>
                <div style="margin: 0 0 0 20px;" id="${name}"></div>
                <div class="deleteAnime">&times;</div>
            </div>
        `;
    }

    setTimeout(() => {
        getAnimeFromLibrery();
        deleteAnimeFromLibrery()
    }, 300);


    // Добавление события для кнопки "Удалить Аниме"
    // Ищем на какую именно кнопку нажали и отправляем название на удаление в Local storage
    function deleteAnimeFromLibrery() {
        let btnFofDelAnime = document.querySelectorAll('.deleteAnime');
        btnFofDelAnime.forEach((element, i) => {
            element.addEventListener('click', () => {
                let div = document.querySelectorAll('div.placeAnime .animeSlot');
                checkTag(div[i].textContent);
            });
        });
    }
    // тут мы ищем аниме, которое хотим удалить и удаляём. Затем обновляем popup
    function checkTag(name) {
        console.log('checkTag')
        console.log(name)
        for (let i = 0; i < localStorage.length; i++) {
            let key = localStorage.key(i);
            try {
                let anime = JSON.parse(localStorage.getItem(key));
                if (anime.name) {
                    if (anime.name.includes(name)) {
                        console.log('checkTAg 2')
                        localStorage.removeItem(key);
                        deleteAnimeInModal(name)

                    }
                }
            } catch (error) {
            }
        }
    }
    // обновление popup
    function deleteAnimeInModal(animeName) {
        let anime = document.querySelectorAll('.placeAnime');
        console.log(anime)
        anime.forEach(element =>{
            let text = element.outerText
            text = text.replace('\n×', '')
            if(text == animeName) {
                console.log('delete anime in modal')
                console.log(element)
                element.remove()
            }
        })
        // animeArr.map(element =>{

        // })
    //     div.innerHTML = `
    //     <div class="modal__close" style="color: white;" >&times;</div>
    //     <div class="modal__title">Библиотека</div>
    // `;
    
    }

    //Modal
    const placeFoModal = document.querySelector('.footer');
    const modalDiv = `
        <div id="myModal" class="modal-ani modalL">
            <div class="modal-content-ani ">
                <div class="modal-header">
                    <span class="close-ani">&times;</span>
                    <h2>ANILIB</h2>
                </div>
                <div class="modal-body modal__content">
                
                </div>
            </div>
        </div>
    `;
    placeFoModal.innerHTML = modalDiv;




    // <div class="modalL">
    // <div class="modal__dialog">
    //     <div class="modal__content">
    //             <div class="modal__close" style="color: white;">&times;</div>
              
    //     </div>
    // </div>
    // </div>

    // setTimeout(() => {
    //     document.querySelector('.modal__close').
    //         addEventListener('click', () => {
    //             closeModal();
    //         });
    // }, 200);

    

    const modal = document.querySelector('.modalL');
    // Get the <span> element that closes the modal
    const closeModal = document.getElementsByClassName("close-ani")[0];

    // When the user clicks on <span> (x), close the modal
    closeModal.onclick = function() {
        modal.style.display = "none";
    }

    window.onclick = function(event) {
        if (event.target == modal) {
          modal.style.display = "none";
        }
    }

    // Создание кнопки "Просмотр библиотеки"
    const checkLibreryBtn = document.createElement('button'); //btn id clb
    checkLibreryBtn.classList.add('checkLibrery');
    checkLibreryBtn.textContent = 'Библеотека';
    // Место куда поставить кнопку
    const placeCLB = document.querySelector('.asidehead');
    placeCLB.appendChild(checkLibreryBtn);
    // Работа с кнопкой "Просмотр библиотеки"
    // открыть библеотку 
    checkLibreryBtn.addEventListener('click', () => {
        modal.style.display = "block";
    });

    // function openModal() {
    //     modal.classList.add('show');
    //     modal.classList.remove('hide');
    // }

    // function closeModal() {
    //     modal.classList.remove('show');
    //     modal.classList.add('hide');
    // }


    // document.addEventListener('keydown', (e) => {
    //     if (e.code === "Escape" && modal.classList.contains('show')) {
    //         closeModal();
    //     }
    // });

