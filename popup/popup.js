(async function () {
  const urls = {
    login: 'http://localhost:4312/auth/users/login',
    reg: 'http://localhost:4312/auth/users',
    getLib: 'http://localhost:4312/lib/get'
  }
  const divReg = document.getElementById('div-reg')
  const divLog = document.getElementById('div-log')
  const divAuth = document.getElementById('auth')
  
  const mainToken = await readStore('token')
  if(mainToken){
    divAuth.classList.add('hide')
  }
    
  const btnHideLogin = document.getElementById('btn-hide-login')
  btnHideLogin.addEventListener('click', (event) => {
    divReg.classList.remove('hide')
    divReg.classList.add('show')
    divLog.classList.remove('show')
  })
  
  const btnHideReg = document.getElementById('btn-hide-reg')
  btnHideReg.addEventListener('click', (event) => {
    divLog.classList.add('show')
    divLog.classList.remove('hide')
    divReg.classList.remove('show')
  })
  
  
  const formLogin = document.getElementById('login')
  formLogin.addEventListener('submit', (e) =>{
    e.preventDefault()
  
    const user = {
      user: {
        email: document.getElementById('login-email').value,
        password: document.getElementById('login-pass').value,
      }
    }
    login(user)
  })
  
  async function login(data) {
    const myHeaders = new Headers();
    myHeaders.append("Content-Type", "application/json");
    const raw = JSON.stringify(data);
    const requestOptions = {
      method: 'POST',
      headers: myHeaders,
      body: raw,
      redirect: 'follow'
    };
    fetch(urls.login, requestOptions)
      .then(response => response.text())
      .then(result => {
        const res = JSON.parse(result)
        const token = `Bearer ${res.user.token}`
        setStore({'token': token})
        mainToken = token
      })
      .catch(error => console.log('error', error));
  }
  
  async function login(data) {
    const myHeaders = new Headers();
    myHeaders.append("Content-Type", "application/json");
    const raw = JSON.stringify(data);
    const requestOptions = {
      method: 'POST',
      headers: myHeaders,
      body: raw,
      redirect: 'follow'
    };
    fetch(urls.reg, requestOptions)
      .then(response => response.text())
      .then(result => {
        const res = JSON.parse(result)
        const token = `Bearer ${res.user.token}`
        setStore({'token': token})
        mainToken = token
      })
      .catch(error => console.log('error', error));
  }
  
  async function getLib(data) {
    const myHeaders = new Headers();
    myHeaders.append("Content-Type", "application/json");
    myHeaders.append("Authorization", token)
    const raw = JSON.stringify(data);
    const requestOptions = {
      method: 'POST',
      headers: myHeaders,
      body: raw,
      redirect: 'follow'
    };
    fetch(urls.getLib, requestOptions)
      .then(response => response.text())
      .then(result => {
        console.log(result)
      })
      .catch(error => console.log('error', error));
  }
  
  
  
  
  
  
  async function setStore({key: value}) {
    return await chrome.storage.sync.set({key: value})
  }
  
  async function readStore(key) {
    return await chrome.storage.sync.get([key])
  }
})
